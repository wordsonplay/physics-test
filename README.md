A comparison of different methods of moving objects in Unity:

1. Rigidbody.AddForce with ForceMode = Force
2.  Rigidbody.AddForce with ForceMode = Impulse (called once at Start)
3. Setting rigidbody.velocity
4. Setting rigidbody.position
5. Rigidbody.MovePosition
6. Setting transform.position with a kinematic rigidbody

Playable demo: [https://wordsonplay.itch.io/physics-test](https://wordsonplay.itch.io/physics-test)