﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class AddForceImpulse : MonoBehaviour {

	private Rigidbody rigidbody;

	public float force = 1;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.AddForce(Vector3.right * force, ForceMode.Impulse);
	}
	

}
