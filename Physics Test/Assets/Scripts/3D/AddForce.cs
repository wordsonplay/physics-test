﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class AddForce : MonoBehaviour {

	private Rigidbody rigidbody;

	public float force = 1;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}

	void FixedUpdate() {
		rigidbody.AddForce(Vector3.right * force);
	}
	

}
