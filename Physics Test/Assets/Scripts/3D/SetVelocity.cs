﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class SetVelocity : MonoBehaviour {

	private Rigidbody rigidbody;

	public float speed = 1;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rigidbody.velocity = Vector3.right * speed;
	}
}
