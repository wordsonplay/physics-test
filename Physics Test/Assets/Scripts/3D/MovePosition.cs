﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePosition : MonoBehaviour {

	private Rigidbody rigidbody;

	public float speed = 1;
	public bool useTransform = false;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate() {
		if (useTransform) {
			// following example in scripting reference
			rigidbody.MovePosition(transform.position + Vector3.right * speed * Time.deltaTime);
		}
		else {
			rigidbody.MovePosition(rigidbody.position + Vector3.right * speed * Time.fixedDeltaTime);
		}
	}
}
