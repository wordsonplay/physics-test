﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class SetPosition : MonoBehaviour {

	private Rigidbody rigidbody;

	public float speed = 1;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate() {
		rigidbody.position += Vector3.right * speed * Time.fixedDeltaTime;
	}
}
