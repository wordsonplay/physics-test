﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class KinematicMove : MonoBehaviour {

	private Rigidbody rigidbody;

	public float speed = 10;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.isKinematic = true;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += Vector3.right * speed * Time.deltaTime;
	}
}
