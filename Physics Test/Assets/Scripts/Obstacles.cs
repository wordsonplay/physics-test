﻿using UnityEngine;
using System.Collections;

public class Obstacles : MonoBehaviour {

	public GameObject[] staticObstacles;
	public GameObject[] rigidbodyObstacles;

	// Use this for initialization
	void Start () {

		staticObstacles = new GameObject[transform.childCount];
		rigidbodyObstacles = new GameObject[transform.childCount];

		for (int i = 0; i < staticObstacles.Length; i++) {
			Transform t = transform.GetChild(i);
			rigidbodyObstacles[i] = t.gameObject;
			staticObstacles[i] = Instantiate(rigidbodyObstacles[i]);
			staticObstacles[i].transform.parent = transform;
			staticObstacles[i].name = rigidbodyObstacles[i].name + "(Static)";
			staticObstacles[i].transform.position = rigidbodyObstacles[i].transform.position;

			Destroy(staticObstacles[i].GetComponent<Rigidbody>());
			Destroy(staticObstacles[i].GetComponent<Rigidbody2D>());

		}

		GUIControl gui = GUIControl.Instance;
		gui.toggleStaticEvent += OnToggleStatic;
		gui.toggleBouncyEvent += OnToggleBouncy;

		SetStatic(gui.IsStatic);
		SetBouncy(gui.IsBouncy);

	}
	
	private void SetStatic(bool isStatic) {
		for (int i = 0; i < staticObstacles.Length; i++) {
			staticObstacles[i].SetActive(isStatic);
			rigidbodyObstacles[i].SetActive(!isStatic);
		}
	}

	public void OnDestroy() {
		GUIControl.Instance.toggleStaticEvent -= OnToggleStatic;
		GUIControl.Instance.toggleBouncyEvent -= OnToggleBouncy;
	}

	private void SetBouncy(bool isBouncy) {
		Collider collider = staticObstacles[0].GetComponent<Collider>();

		if (collider != null) {
			collider.sharedMaterial.bounciness = isBouncy ? 1 : 0;
		}
		else {
			Collider2D collider2d = staticObstacles[0].GetComponent<Collider2D>(); 
			collider2d.sharedMaterial.bounciness = isBouncy ? 1 : 0; 
		}		
	}

	public void OnToggleStatic(bool isStatic) {
		SetStatic(isStatic);
	}

	public void OnToggleBouncy(bool isBouncy) {
		SetBouncy(isBouncy);
	}

}
