﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GUIControl : MonoBehaviour {

	static private GUIControl instance;

	static public GUIControl Instance {
		get {
			return instance;
		}
	}

	public Text runButtonName;
	public Text threeDButtonName;

	private bool isStatic = true;
	private bool isBouncy = false;
	private bool is3D = true;
	private bool running = false;
	private bool stepping = true;

	public delegate void ToggleModeHandler(bool isOn);
    public ToggleModeHandler toggleStaticEvent;
    public ToggleModeHandler toggleBouncyEvent;

	public bool Running {
		get {
			return running;
		}

		set {
			running = value;
			Time.timeScale = running ? 1 : 0;
			runButtonName.text = running ? "Stop" : "Run";
		}
	}

	public bool Is3D {
		get {
			return is3D;
		}

		set {
			is3D = value;
			threeDButtonName.text = is3D ? "Go 2D" : "Go 3D";
			SceneManager.LoadScene(is3D ? "Scenes/3D" : "Scenes/2D");
		}
	}

	public bool IsStatic {
		get {
			return isStatic;
		}

		set {
			isStatic = value;
			toggleStaticEvent(isStatic);
		}
	}

	public bool IsBouncy {
		get {
			return isBouncy;
		}

		set {
			isBouncy = value;
			toggleBouncyEvent(isBouncy);
		}
	}


	void Awake() {
		instance = this;
		DontDestroyOnLoad(gameObject);		
		Running = false;
		Is3D = true;
	}

	public void ToggleRun() {
		Running = !Running;
	}

	public void Step() {
		stepping = true;
		Running = true;
	}

	public void Reset() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void Toggle3D() {
		Is3D = !Is3D;
		Running = false;
	}

	public void ToggleStatic() {
		IsStatic = !IsStatic;
	}

	public void ToggleBouncy() {
		IsBouncy = !IsBouncy;
	}

	void Update () {		
		if (stepping) {
			stepping = false;
			Running = false;
		}	
	}
}
