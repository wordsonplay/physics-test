﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class SetVelocity2D : MonoBehaviour {

	private Rigidbody2D rigidbody;

	public float speed = 1;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rigidbody.velocity = Vector3.right * speed;
	}
}
