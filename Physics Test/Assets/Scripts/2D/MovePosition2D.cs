﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class MovePosition2D : MonoBehaviour {

	private Rigidbody2D rigidbody;

	public float speed = 1;
	public bool useTransform = false;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();
	}
	
	void FixedUpdate() {
		if (useTransform) {
			// following example in scripting reference
			rigidbody.MovePosition(transform.position + Vector3.right * speed * Time.deltaTime);
		}
		else {
			rigidbody.MovePosition(rigidbody.position + Vector2.right * speed * Time.fixedDeltaTime);
		}
	}
}
