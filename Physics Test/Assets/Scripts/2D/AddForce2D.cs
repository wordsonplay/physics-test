﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class AddForce2D : MonoBehaviour {

	private Rigidbody2D rigidbody;

	public float force = 1;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate() {
		rigidbody.AddForce(Vector3.right * force);
	}
	

}
