﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class AddForceImpulse2D : MonoBehaviour {

	private Rigidbody2D rigidbody;

	public float force = 1;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();
		rigidbody.AddForce(Vector3.right * force, ForceMode2D.Impulse);
	}
	

}
