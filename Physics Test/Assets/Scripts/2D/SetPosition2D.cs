﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class SetPosition2D : MonoBehaviour {

	private Rigidbody2D rigidbody;

	public float speed = 1;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();
	}
	
	void FixedUpdate() {
		rigidbody.position += Vector2.right * speed * Time.fixedDeltaTime;
	}
}
